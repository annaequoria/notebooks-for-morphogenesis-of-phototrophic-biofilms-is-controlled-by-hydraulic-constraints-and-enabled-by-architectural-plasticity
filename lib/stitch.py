import numpy as np
from ipywidgets import widgets,interact,IntProgress
import matplotlib.pyplot as plt
from skimage import morphology
from skimage.morphology import closing, square, reconstruction
from skimage import filters
from PIL import Image
import cv2
from numba import jit

def find_ol(image1,image2, ol,a):
    sx,sy=image1.shape
    im=image1[:,int((1-ol)*sy):].astype('uint8')
    temp=image2[int(a*sx):int(-a*sx),:int((1-ol)*sy)].astype('uint8')
    res = cv2.matchTemplate(img1,temp,eval('cv2.TM_CCOEFF_NORMED'))
    min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)
    h=(max_loc[1]-int(sx*a),max_loc[0]+int(sy*(1-ol)))
    return h
