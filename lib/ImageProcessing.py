import numpy as np
from ipywidgets import widgets,interact,IntProgress
import matplotlib.pyplot as plt
from skimage import morphology
from skimage.morphology import closing, square, reconstruction
from skimage import filters
from PIL import Image
import cv2
from numba import jit



def f1(a):
    try:
        x=a.shape-np.where(a)[0][0]
    except:
        x=0
    return x
    
def f2(a):
    try:
        x=a.shape-np.where(a)[0][-1]
    except:
        x=0
    return x
        
def CalcZMap(image, thr,thr1, small_obj):
    mask=morphology.remove_small_objects(image>thr,small_obj)    #exclude objects smaller than 30 pixels, can be 
#customized
    Z=np.squeeze(np.apply_along_axis(f1,1, mask))
    #if np.sum(Z==0)>100:
    for u in np.arange(thr-thr1):
        if np.sum(Z==0)>50:
            mask1=morphology.remove_small_objects(image>(thr-u),small_obj)    #exclude objects smaller than 30 pixels, can be 
            ZB=np.squeeze(np.apply_along_axis(f1,1, mask1))
            Z[Z==0]=ZB[Z==0]
        else:
            continue
#else:
    #    continue
#     z2=np.squeeze(np.apply_along_axis(f2,1, mask))
    z2=Z
    V=np.sum(mask)    #calc volume as sum of 1 in mask, excluding plexi
    H=np.mean(Z[Z>0])
    R=np.std(Z[Z>0])/H
    return mask,Z,V,H,R,z2

def CalcZMap_old(image, thr,thr1, small_obj):
    mask=morphology.remove_small_objects(image>thr,small_obj)    #exclude objects smaller than 30 pixels, can be 
#customized
    Z=np.squeeze(np.apply_along_axis(f1,1, mask))
#     z2=np.squeeze(np.apply_along_axis(f2,1, mask))
    z2=Z
    V=np.sum(mask)    #calc volume as sum of 1 in mask, excluding plexi
    H=np.mean(Z[Z>0])
    R=np.std(Z[Z>0])/H
    return mask,Z,V,H,R,z2


def find_ol(image1,image2,ol,a):
    sx,sy=image1.shape
    im=image1[:,int((1-(ol+0.05))*sy):].astype('uint8')
    temp=image2[int(a*sx):int(-a*sx),:int((ol-0.1)*sy)].astype('uint8')
    res = cv2.matchTemplate(im,temp,eval('cv2.TM_CCOEFF_NORMED'))
    min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)
    h=(max_loc[1]-int(sx*a),max_loc[0]+int((1-(ol+0.05))*sy))
    return h

def find_ol_n(image1,image2,ol,a):
    sx,sy=image1.shape
    im=image1[:,int((1-(ol))*sy):].astype('uint8')
    temp=image2[int(a*sx):int(-a*sx),:int((ol)*sy)].astype('uint8')
    res = cv2.matchTemplate(im,temp,eval('cv2.TM_CCOEFF_NORMED'))
    min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)
    h=(max_loc[1]-int(sx*a),max_loc[0]+int((1-(ol))*sy))
    return h

def find_ol0(image1,image2,ol,a):
    ol=ol-0.1
    sy,sx=image1.shape
    #a=int(sy*0.15)
    img1=image1[:,int(sx*(1-ol)):].astype('uint8')
    temp=image2[int(sy*a):-int(sy*a),:int(ol*sx)].astype('uint8')
    res = cv2.matchTemplate(img1,temp,eval('cv2.TM_CCOEFF_NORMED'))
    min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)
    h=(max_loc[1]-int(sy*a),max_loc[0]-10+int(sx*(1-ol)))
    return h


def find_ol1(image1,image2,ol,a):
    sy,sx=image1.shape
   # a,b=int(sy*0.15), int(sx*0.05)
    img1=image1.astype('uint8')
    temp=image2[int(sy*a):-int(sy*a),:int(ol*sx)].astype('uint8')
    res = cv2.matchTemplate(img1,temp,eval('cv2.TM_CCORR_NORMED'))
    min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)
    h=(max_loc[1]-int(sy*a),max_loc[0])
    return h

def ol_mat(M, ol,a):
    n_rows,n_cols, sy,sx=M.shape
    OL=np.full((2,n_rows,n_cols),0)
    for row in range(n_rows):
        for col in range(n_cols-1):
            OL[0,row,col+1]=int(find_ol(M[row,col,:,:],M[row,col+1,:,:],ol,a)[0])
            OL[1,row,col+1]=int(find_ol(M[row,col,:,:],M[row,col+1,:,:],ol,a)[1])
   
    OLn=OL.copy()
    for row in range(n_rows):
        OLn[1,row,:]=np.asarray([int(sum(OL[1,row,:i+1])) for i in range(n_cols)])
    return OLn

def stitch_new(M,ol,a):
    OL=ol_mat(M,ol,a)
    n_rows,n_cols, sx,sy=M.shape
    b=int(0.04*sx)
    sh_x,sh_y=OL[:,:,:]
    M1=np.full((n_rows,1,np.min(sh_x)+sx-np.max(sh_x),np.max(sh_y)+sy),0)
    #bx=int(0.01*sx)
    
    for row in range(n_rows):
        Z=np.zeros((sx+2*b,int(sy*n_cols)))
        for col in range(n_cols):
            ol_x,ol_y=OL[:,row,col]
            im=M[row,col,:,:]
            Z[b+int(ol_x):b+int(ol_x)+im.shape[0],int(ol_y):int(ol_y+im.shape[1])]=im
        
        M1[row,0,:,:]=Z[b+np.max(sh_x):b+np.min(sh_x)+sx,:np.max(sh_y)+sy]
    return M1


def Stitch(M,ol,a):
    M1=stitch(M,ol,a)
    M2=np.rot90(np.rot90(M1,axes=(2,3)), axes=(0,1))
    M3=stitch(M2,ol,a)
    Z=M3[0,0,:,:]
    return np.rot90(Z,k=3)


def stitch(M,OL,OLv,crop):
    n_rows,n_cols, sy,sx=M.shape
    #Z=np.zeros((int(sy*n_rows*(1-ol)),int(sx*n_cols*(1-ol))))
    Z=np.zeros((int(sy*n_rows),int(sx*n_cols)))
    
    for row in range(n_rows):
        for col in range(n_cols):
            ol_Y=OLv[1,col,row]+crop
            ol_x=OL[1,row,col]+crop
            im=M[row,col,crop:-crop,crop:-crop]
            Z[int(ol_Y):int(ol_Y+im.shape[0]),int(ol_x):int(ol_x+im.shape[1])]=im
    Z=Z[:int(min(OLv[1,:,n_rows-1])+crop+im.shape[0]),:int(min(OL[1,:,n_cols-1])+crop+im.shape[1])]
    return Z

@jit
def movWin(img,win,fun):
    T=np.zeros(img.shape)
    for y in range(win,img.shape[1]-win):
            for x in range(win,img.shape[0]-win):
                patch=img[y-win:y+win,x-win:x+win]
                T[y,x]=fun(patch)
    return T
    