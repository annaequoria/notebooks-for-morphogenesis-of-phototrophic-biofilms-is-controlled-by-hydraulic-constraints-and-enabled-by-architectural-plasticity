import sys,json
import OctControl
import numpy as np
import GRBLserver
stp=GRBLserver.grbl(url='/stepcraft',conName='')

import time
from time import gmtime, strftime

#acquire image in the current position
def makeScan(stp_position='default',params={}):
    #OctControl.setParameters(**params)
    params['colorBoundaries']=[20.0,90.0]
    #OctControl.setParameters(colorBoundaries=[20.0,80.0])
    params['path']="../Measurments/%Y-%m-%d_%H-%M-%S_"+stp_position
    #OctControl.setParameters(path="../Measurments/%Y-%m-%d"+stp_position)
    params['exportRAWGrayscale8bit']="%Y-%m-%d_%H-%M-%S_"+stp_position
    #OctControl.setParameters(exportRAWGrayscale8bit="%Y-%m-%d"+stp_position)
    params['exportSRM']="%Y-%m-%d_%H-%M-%S_"+stp_position
   # OctControl.setParameters(exportSRM="%Y-%m-%d"+stp_position)    
    #OctControl.setParameters(path="../Measurments/%Y-%m-%d/%Y-%m-%dbla_"+stp_position)
    #OctControl.setParameters(exportRAWGrayscale8bit="%Y-%m-%d_"+stp_position)
    #OctControl.setParameters(exportSRM="%Y-%m-%d_"+stp_position)
    print('here1')
    try:
        print('here2')
        stp.actualizeStatus()
        print('here3')
        params['stepcraftStatus']=stp.status
        print('here4')
        ##new
        meta=OctControl.scan(**params)
        with open(meta['path_raw8bit'].replace('.raw','.json'), 'w') as fp:
            json.dump(meta, fp)
   
    except:
        print('cannot set stepcraft status')
        
    #meta=OctControl.scan(**params)
    
    #with open(meta['path_raw8bit'].replace('.raw','.json'), 'w') as fp:
     #   json.dump(meta, fp)
    

#acquire image in position defined in pos
def makeMoveScan(pos=None,positions='default',stp_position='default',params={}):

    if pos is None:
        pos=positions[stp_position]
    stp.moveTo(z=-0.5)
    stp.moveTo(x=pos[0],y=pos[1])
    stp.moveTo(z=pos[2])
    #print('scan '+stp_position+' @'+str(pos))
    stp.waitFinish()
    makeScan(stp_position,params=params)
    stp.moveTo(z=-0.5)


def makeMoveScan_nolift(pos=None,positions='default',stp_position='default',params={}):
    x,y,z=pos
    if pos is None:
        x,y,z=positions[stp_position]
    stp.moveTo(z=-40)
    stp.moveTO(x=x)
    stp.moveTO(y=y)
    stp.moveTO(z=z)
    print('here6')
    stp.waitFinish()
    print('here7')
    makeScan(stp_position,params=params)

def makeMoveScan_nolift1(pos=None,positions='default',stp_position='default',params={}):
    x,y,z=pos
    if pos is None:
        x,y,z=positions[stp_position]
#     stp.moveTo(z=-10)
    stp.moveTO(x=x)
    stp.moveTO(y=y)
    stp.moveTO(z=z)
    print('here6')
    stp.waitFinish()
    print('here7')
    makeScan(stp_position,params=params)

#time execution
def tic():
    import time
    global startTime_for_tictoc
    startTime_for_tictoc = time.time()

def toc():
    import time
    if 'startTime_for_tictoc' in globals():
        t=time.time();
        print("Elapsed time is " + str(time.time() - startTime_for_tictoc) + " seconds.")  
    else:
        print("Toc: start time not set")
