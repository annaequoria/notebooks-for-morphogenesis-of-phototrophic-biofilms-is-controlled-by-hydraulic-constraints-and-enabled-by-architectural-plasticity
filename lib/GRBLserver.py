from websocket import create_connection
import json
import time
import numpy as np
import operator
import re
import serial


STATUSPAT = re.compile(r"^<(\w*?),MPos:([+\-]?\d*\.\d*),([+\-]?\d*\.\d*),([+\-]?\d*\.\d*),WPos:([+\-]?\d*\.\d*),([+\-]?\d*\.\d*),([+\-]?\d*\.\d*),?(.*)>$")

class grbl:
    def __init__(self, conName="localhost",port=5020,url=''):

            
        self.conn = create_connection("ws://"+conName+":"+str(port)+url+"/ws")
        self.conn.settimeout(1.0)
        self.status=None
        
        
        if self.conn.recv()=='ConnectionOK':
            print('ConnectionOK')
        
        self.setStartPosition()
        
    def setStartPosition(self):
        self.actualizeStatus()
        self.destPos=self.status['MPosOffs']
        
    def close(self):
        self.conn.close()
        self.state="undef"
        
    def sendCmd(self,cmd,feedback=False):
        self.conn.send(json.dumps({'data':cmd,'feedback':feedback}))
        if feedback:
            return self.conn.recv()

    def actualizeStatus(self):
        
        self.conn.send('{"srvCmd":"enableStatusNext"}')
        retString=self.conn.recv()
        try:
            self.status=json.loads(retString)
        except ValueError:
            print('cannot decode JSON '+retString)
            
        while not 'State' in self.status.keys():
            time.sleep(0.2)
            print('State was not in status')
            self.actualizeStatus()
                
    def moveTo(self,x=None,y=None,z=None,rel=False):
        cmd="G0"
        if not x is None:
            self.destPos[0]=x;
            cmd=cmd+"X%1.3f"%x;
        if not y is None:
            self.destPos[1]=y;
            cmd=cmd+"Y%1.3f"%y;
        if not z is None:
            self.destPos[2]=z;
            cmd=cmd+"Z%1.3f"%z;
        self.conn.send(json.dumps({'gCode':cmd}))
        print(cmd)
        
    def moveTO(self,x=None,y=None,z=None,rel=False,shootOff=1.0):
        cmd1="G0"
        cmd2="G0"
        if not x is None:
            x1=x+shootOff
            self.destPos[0]=x;
            cmd1=cmd1+"X%1.3f"%x1;
            cmd2=cmd2+"X%1.3f"%x;
        if not y is None:
            y1=y+shootOff
            self.destPos[1]=y;
            cmd1=cmd1+"Y%1.3f"%y1;
            cmd2=cmd2+"Y%1.3f"%y;
        if not z is None:
            z1=min(z+shootOff,-0.5)
            self.destPos[2]=z;
            cmd1=cmd1+"Z%1.3f"%z1;
            cmd2=cmd2+"Z%1.3f"%z;
        self.conn.send(json.dumps({'gCode':cmd1}))
  #      print(cmd1)
  #      self.waitFinish()
        self.conn.send(json.dumps({'gCode':cmd2}))
        print(cmd2)
        
    def selectOffset(self,work=None,tool=None):
        data={"func":"selectOffset"}
        if not work is None:
            data['workOffset']=work
        if not tool is None:
            data['toolOffset']=tool
        #print(json.dumps(data))
        self.conn.send(json.dumps(data))
        
    def stop(self):
        self.conn.send(json.dumps({'stop':''}))
        
    def homing(self):
        try:
            self.sendCmd('$H')
        except WebSocketTimeoutException:
            pass
        
    def waitFinish(self):
        try:
            self.actualizeStatus()
            maxDist=1;
            while self.status['State']!="Idle" or maxDist>0.01:
                time.sleep(0.25)
                self.actualizeStatus()
                #maxDist=0
                #print(self.status['MPos'])
                #print(self.destPos)
                maxDist= np.max(np.absolute((self.status['MPosOffs'][0]-self.destPos[0],self.status['MPosOffs'][1]-self.destPos[1],self.status['MPosOffs'][2]-self.destPos[2])))
                #print(maxDist)
        except KeyboardInterrupt:
            print("KeyboardInterrupt -> Stop")
            self.stop()

    def waitFinishdsf(self):
        
        maxDist=1;
        while self.status['State']!="Idle" or maxDist>0.02:
            time.sleep(0.25)
            self.actualizeStatus()
            #maxDist=0
            #print(self.status['MPos'])
            #print(self.destPos)
            maxDist= np.max(np.absolute((self.status['MPosOffs'][0]-self.destPos[0],self.status['MPosOffs'][1]-self.destPos[1],self.status['MPosOffs'][2]-self.destPos[2])))
            #print(maxDist)
    #def waitFinish(self):
     #   try:
      #      self.actualizeStatus()
       #     maxDist=1;
        #    while self.status['State']!="Idle" or maxDist>0.01:
         #       time.sleep(0.25)
          #      self.actualizeStatus()
                #maxDist=0
                #print(self.status['MPos'])
                #print(self.destPos)
              #  maxDist= np.max(np.absolute((self.status['MPosOffs'][0]-self.destPos[0],self.status['MPosOffs'][1]-self.destPos[1],self.status['MPosOffs'][2]-self.destPos[2])))
           #     maxDist= np.max(np.absolute((self.status['MPos'][0]-self.destPos[0],self.status['MPos'][1]-self.destPos[1],self.status['MPos'][2]-self.destPos[2])))
                #print(maxDist)
        #except KeyboardInterrupt:
         #   print("KeyboardInterrupt -> Stop")
          #  self.stop()
    




