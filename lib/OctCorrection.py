import numpy as np
from skimage.filters import laplace, prewitt_v
from skimage.morphology import remove_small_objects
import scipy

def correct_new(image, z_of_plexi=50, ulim=860,llim=1000, smooth=15000, bckHeight=100,prewitt_thr=0.07):
    im=np.flip(image,axis=2)
    b=np.mean(im[:,:,:bckHeight]);     #calculate background
    Z=np.zeros((im.shape[0],im.shape[1]))
    for z in range(im.shape[0]):
        image=im[z,:,:]
        mask=remove_small_objects(prewitt_v(image)>prewitt_thr,10)
        Z[z,:]=np.argmax(mask, axis=1)
    Z[Z<ulim]=0
    Z[Z>llim]=0
    x,y=np.where(Z>0)
    z=Z[Z>0].ravel()
    f=scipy.interpolate.bisplrep(x[::10],y[::10],z[::10], kx=2, ky=2, s=smooth)
    Z1=scipy.interpolate.bisplev(np.arange(Z.shape[0]),np.arange(Z.shape[1]),f)
    image_corr=np.zeros(im.shape)
    XX,YY=np.meshgrid(range(im.shape[0]),range(im.shape[1]))
    for (x1,y1) in list(zip(XX.ravel(),YY.ravel())):
        shift=int(Z1[x1,y1])
        image_corr[x1,y1,-shift-z_of_plexi:]=im[x1,y1,:shift+z_of_plexi]
    image_corr1=np.zeros(image_corr.shape, dtype=np.uint8);     #subtract background
    image_corr1[image_corr>int(b)]=image_corr[image_corr>int(b)]-int(b)
    return np.rot90(image_corr1, axes=(2,1))


def correct_new_pad(image, z_of_plexi=50, ulim=860,llim=1000, smooth=15000, bckHeight=100,prewitt_thr=0.07, small_obj=10,skip=10):
    im=np.flip(image,axis=2)
    b=np.mean(im[:,:,:bckHeight]);     #calculate background
    Z=np.zeros((im.shape[0],im.shape[1]))
    for z in range(im.shape[0]):
        image=im[z,:,:]
        mask=remove_small_objects(prewitt_v(image)>prewitt_thr,small_obj)
        Z[z,:]=np.argmax(mask, axis=1)
    Z[Z<ulim]=0
    Z[Z>llim]=0
    x,y=np.where(Z>0)
    z=Z[Z>0].ravel()
    f=scipy.interpolate.bisplrep(x[::skip],y[::skip],z[::skip], kx=2, ky=2, s=smooth)
    Z1=scipy.interpolate.bisplev(np.arange(Z.shape[0]),np.arange(Z.shape[1]),f)
    im_pad=np.pad(im, ((0,0),(0,0),(0,z_of_plexi)), mode='constant', constant_values=(0))
    image_corr=np.zeros(im.shape)
    XX,YY=np.meshgrid(range(im.shape[0]),range(im.shape[1]))
    for (x1,y1) in list(zip(XX.ravel(),YY.ravel())):
        shift=int(Z1[x1,y1])
        column=im_pad[x1,y1,bckHeight*2:shift+z_of_plexi]
        image_corr[x1,y1,-(column.shape[0]):]=column
    #image_corr=image_corr[:,:,:-z_of_plexi+50]
    image_corr1=np.zeros(image_corr.shape, dtype=np.uint8);     #subtract background
    image_corr1[image_corr>int(b)]=image_corr[image_corr>int(b)]-int(b)
    return np.rot90(image_corr1, axes=(2,1))

def correct_new_pad_no_bcksub(image, z_of_plexi=50, ulim=860,llim=1000, smooth=15000, bckHeight=100,prewitt_thr=0.07, small_obj=10,skip=10):
    im=np.flip(image,axis=2)
    b=np.mean(im[:,:,:bckHeight]);     #calculate background
    Z=np.zeros((im.shape[0],im.shape[1]))
    for z in range(im.shape[0]):
        image=im[z,:,:]
        mask=remove_small_objects(prewitt_v(image)>prewitt_thr,small_obj)
        Z[z,:]=np.argmax(mask, axis=1)
    Z[Z<ulim]=0
    Z[Z>llim]=0
    x,y=np.where(Z>0)
    z=Z[Z>0].ravel()
    f=scipy.interpolate.bisplrep(x[::skip],y[::skip],z[::skip], kx=2, ky=2, s=smooth)
    Z1=scipy.interpolate.bisplev(np.arange(Z.shape[0]),np.arange(Z.shape[1]),f)
    im_pad=np.pad(im, ((0,0),(0,0),(0,z_of_plexi)), mode='constant', constant_values=(0))
    image_corr=np.zeros(im.shape)
    XX,YY=np.meshgrid(range(im.shape[0]),range(im.shape[1]))
    for (x1,y1) in list(zip(XX.ravel(),YY.ravel())):
        shift=int(Z1[x1,y1])
        column=im_pad[x1,y1,bckHeight*2:shift+z_of_plexi]
        image_corr[x1,y1,-(column.shape[0]):]=column
    #image_corr=image_corr[:,:,:-z_of_plexi+50]
    return np.rot90(image_corr, axes=(2,1))
