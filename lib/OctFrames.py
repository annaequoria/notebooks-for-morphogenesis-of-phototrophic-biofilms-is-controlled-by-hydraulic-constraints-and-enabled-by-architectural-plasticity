import numpy as np
from PIL import Image

import json
import medfilt
import matplotlib.pyplot as plt
import os


class OctFrames():
    
    def __init__(self, meta=None,jsonFile=""):
        self.folderName=os.path.dirname(jsonFile)+'/'
        if meta is None:
            meta=json.load(open(jsonFile, 'r'))
        self.meta=meta
        self.poly2ndDegre=None
        self.frameSeq=[]
        self.axisDir=meta['axisDir']
        self.imgShape=meta['imgShape']
        self.imgRange=meta['imgRange']
        
        imageIndex=[]
        dtype=None

        dtype=np.uint8
        self.filename = self.folderName+meta['path_raw8bit']
        rawFile=open(self.filename, 'r')
        self.frameSeq = np.fromfile(rawFile, dtype=dtype).reshape(meta['imgShape'])
        print("import "+self.filename)
    def swapaxes(self,a,b):
        self.frameSeq=self.frameSeq.swapaxes(a,b)
                            
        self.imgShape[a],self.imgShape[b]=self.imgShape[b],self.imgShape[a]
        self.imgRange[a],self.imgRange[b]=self.imgRange[b],self.imgRange[a]
        self.axisDir[a],self.axisDir[b]=self.axisDir[b],self.axisDir[a]

    def volume(self,ind):
        return self.imgRange[ind][1]-self.imgRange[ind][0]
    
    def indToPos(self,tst=None,x=None,y=None,z=None):
        if tst is not None:
            raise ValueError
        if x is not None:
            ind=2
            values=x
        elif y is not None:
            ind=1
            values=y
        elif z is not None:
            ind=0
            values=z
        else:
            print('define a value x,y or z')
            return 
        return values/float(self.imgShape[ind])*self.volume(ind)+self.imgRange[ind][0]
        
    def posToInd(self,tst=None,x=None,y=None,z=None):
        if tst is not None:
            raise ValueError
        if x is not None:
            ind=2
            values=x
        elif y is not None:
            ind=1
            values=y
        elif z is not None:
            ind=0
            values=z
        else:
            print('define a value x,y or z')
            return 
        return ((values-self.imgRange[ind][0])*float(self.imgShape[ind])/self.volume(ind)).astype(int)
        
        
     
    def cropRange(self,x=None,y=None,z=None):
        cropRange=[x,y,z]  
        for ind in range(0,len(cropRange)):
            if cropRange[ind] is None:
                cropRange[ind]=range(0,self.imgShape[ind])
                
            if cropRange[ind].step > 0:
               cropRange[ind].stop=max(cropRange[ind])
            else:
                cropRange[ind].stop=min(cropRange[ind])
            
            self.imgRange[ind][0]=self.volume(ind)/float(self.imgShape[ind])*float(cropRange[ind].start)
            self.imgRange[ind][1]=self.volume(ind)/float(self.imgShape[ind])*float(cropRange[ind].stop+1)

            
        for ind in range(cropRange[2]):
            self.frameSeq[ind]=self.frameSeq[ind][cropRange[0]][cropRange[1]]
            
        self.imgShape = [self.frameSeq[0].shape[0],self.frameSeq[0].shape[1],len(self.frameSeq)]
        
    def extractPolynomial(self,ind=0,polyOrder=2,exportData=False,tol=0.1,plot=False,greyScaleTol=50,medFiltTol=0.03,medFiltLen=21,polyFitTol=0.005,minValidRatio=0.1):

        maxLine=[]
        imarray=self.frameSeq[ind]
        imarrayRot=imarray.swapaxes(1,0)
        maxRange=[]
        
        if plot:
            plt.imshow(imarray, cmap="gray")
            rmX=[]
            rmY=[]
        xPos=0
        
        grayScaleDiffList=[]
        #Extract maximum Pixel
        for line in imarrayRot:
            maxInd=line.argmax()
            yAct=self.indToPos(y=float(line.argmax()))
            xAct=self.indToPos(x=xPos)
            grayScaleDiff=line[maxInd]-line.mean()
            grayScaleDiffList.append(grayScaleDiff)
            if grayScaleDiff>greyScaleTol:
                maxLine.append(yAct)
                maxRange.append(xAct)
            elif plot:
                rmX.append(xAct)
                rmY.append(yAct)
            xPos+=1

        maxLine=np.array(maxLine)
        maxRange=np.array(maxRange)
        
        if plot:
            plt.plot(self.posToInd(x=np.array(rmX)),self.posToInd(y=np.array(rmY)),'b.')
            
            rmX=[]
            rmY=[]
            
        #Median Filter
        if not medFiltTol is None:
            if len(maxLine)<medFiltLen:
                if plot:
                    plt.show()
                    plt.plot(range(len(grayScaleDiffList)),grayScaleDiffList)
                    plt.show()
                print("Not engoth data to filter: " + str(len(maxLine))+"<"+str(medFiltLen))
                return None
            flt=medfilt.medfilt(maxLine,medFiltLen)
            valid=np.abs(maxLine-flt)<medFiltTol

            if plot:
                rmX=maxRange[~valid]
                rmY=maxLine[~valid]
                plt.plot(self.posToInd(x=np.array(rmX)),self.posToInd(y=np.array(rmY)),'r.')
            
            maxLine=maxLine[valid]
            maxRange=maxRange[valid]
        
            if float(len(maxLine))/float(imarray.shape[1])<minValidRatio:
                print("Median Filer fail")
                if plot:
                    plt.show()
                return None
        
        # Filter to polyLine
        if not polyFitTol is None:    
            poly=np.poly1d(np.polyfit(maxRange,maxLine,polyOrder))
            keepRange=abs(maxLine-poly(maxRange))<polyFitTol
            if plot:
                rmX=maxRange[~keepRange]
                rmY=maxLine[~keepRange]
                plt.plot(self.posToInd(x=rmX),self.posToInd(y=rmY),'y.')
            maxRange=maxRange[keepRange]
            maxLine=maxLine[keepRange]
            
            if float(len(maxLine))/float(imarray.shape[1])<minValidRatio:
                print("Filter to polyLine fail at ind: "+str(ind))

                if plot:
                    xRange=np.array(range(0,imarray.shape[1]))
                    plt.plot(xRange,self.posToInd(y=poly(self.indToPos(x=xRange))),'m')
                    plt.show()
                return None

            

        
        self.poly=np.poly1d(np.polyfit(maxRange,maxLine,polyOrder))
        
        if plot:
            plt.plot(self.posToInd(x=maxRange),self.posToInd(y=maxLine),'g.')
            xRange=np.array(range(0,imarray.shape[1]))
            plt.plot(xRange,self.posToInd(y=self.poly(self.indToPos(x=xRange))),'m')
            plt.show()
            
        if float(len(maxLine))/float(imarray.shape[1])<minValidRatio:
            
            return None
        return self.poly
    
    
    def extract3dPolynomial(self,polyOrder=2,zRange=None):
        poly2ndDegre=[]
        if zRange is None:
            zRange=range(0,len(self.frameSeq))
            
        plexiPolyList=[]  
        for ind in zRange:
            plexiPolyList.append(self.extractPolynomial(ind,polyOrder))
            #print("ind:"+str(ind)+"\n  "+str(plexiPolyList[-1]))
            
        for order in range(polyOrder+1):
            coeffsList=[]
            indCoeffs=0
            validZRange=[]
            for ind in zRange:
                coeffs=plexiPolyList[indCoeffs]
                if not coeffs is None:
                    try:
                        coeffsList.append(coeffs.coeffs[order])
                        validZRange.append(ind)
                    except IndexError:
                        pass
                indCoeffs+=1
            validZRange=np.array(validZRange)
            coeffsList=np.array(coeffsList)
        
            poly2ndDegre.append(np.poly1d(np.polyfit(self.indToPos(z=validZRange),coeffsList,polyOrder)))
        self.poly2ndDegre=poly2ndDegre
        return poly2ndDegre
    def getPoly(self,poly2ndDegre=None,ind=0,zPos=None):
        
        if zPos is None:
            zPos=self.indToPos(z=ind)
        
        if poly2ndDegre is None:
            poly2ndDegre=self.poly2ndDegre
    
        polyOrder=len(poly2ndDegre)-1
        coeffsList=[]
        for order in range(polyOrder+1):
            coeffsList.append(poly2ndDegre[order](zPos))
        return np.poly1d(coeffsList)
         
         
    def correctImageNew(self,destImage,poly=None,ind=0, yMove=None,yArray=None):
        imarray=self.frameSeq[ind]
        sizeY,sizeX=imarray.shape
        xImgRange=np.arange(0,sizeX)
        if poly is None:
            self.getPoly(ind=ind)
        if yArray is None:
            yArray=[0,sizeY]
        
        #
        
        yMovArray=self.posToInd(y=poly(self.indToPos(x=xImgRange)))

        #empytyVal=imarray.mean()
        empytyVal=0
        if yMove is None:
            yMove=int(yMovArray.mean())
            print("yMove: "+str(yMove)) 
        yMovArray-=yMove

        for x in range(sizeX):
            decaly= yMovArray[x]
            a=yArray[0]+decaly
            b=yArray[1]+decaly
            try:
                destImage[ind,:,x] =imarray[a:b,x]
            except ValueError as e:
                
                if(a<0):
                    acut=-a
                else:
                    acut=0
                if(b>sizeY):
                    bcut=b-sizeY
                else:
                    bcut=0
                asour=a+acut
                bsour=b-bcut
                adest=acut
                bdest=yArray[1]-yArray[0]-bcut
                
                try:
                    destImage[ind,adest:bdest,x] =imarray[asour:bsour,x]
                except ValueError as e:
                    print("@x: "+str(x))
                    print("a: "+str(a))
                    print("b: "+str(b))
                    print("asour: "+str(asour))
                    print("adest: "+str(adest))
                    print("bsour: "+str(bsour))
                    print("bdest: "+str(bdest))
                    break
        return yMove  
    def correct3dImage(self,poly2ndDegre=None, yMove=None,yArray=None):
    
        
        sizeY,sizeX=self.frameSeq[0].shape
        
        if yArray is None:
            yArray=[0,sizeY]
    
        destImage=np.zeros([len(self.frameSeq),yArray[1]-yArray[0],sizeX],self.frameSeq.dtype)
        
        if poly2ndDegre is None:
            poly2ndDegre=self.poly2ndDegre
            
        for ind in range(len(self.frameSeq)):
            poly=self.getPoly(poly2ndDegre,ind)
            
            yMove=self.correctImageNew(destImage,ind=ind,yMove=yMove,poly=poly,yArray=yArray)
        if not yArray is None:
            self.imgRange[1]=self.indToPos(y=np.array(yArray))
            self.imgShape[1]=yArray[1]-yArray[0]
        self.frameSeq=destImage
        return yMove
        
    def removePolyLine(self,poly=None,ind=0,width=5,offset=0):
        
        if poly is None:
            poly=self.getPoly(ind=ind)
    
        imarray=self.frameSeq[ind]
        xRange=np.array(range(0,imarray.shape[1]))
        yPos=self.posToInd(y=poly(self.indToPos(x=xRange)))+offset
        for x in xRange:
            y=yPos[x]
            yLow=np.min([np.max([y-width,0]),imarray.shape[0]]);
            yHigh=np.min([np.max([y+width,0]),imarray.shape[0]]);
            imarray[yLow:yHigh,x]=0
            
    def remove3dPolyLine(self,poly2ndDegre=None,ind=0,width=5,offset=0):
        if poly2ndDegre is None:
            poly2ndDegre=self.poly2ndDegre
            
        for ind in range(len(self.frameSeq)):
            poly=self.getPoly(poly2ndDegre,ind)
            self.removePolyLine(poly=poly,ind=ind,width=width,offset=offset)
            
    def exportRaw(self,fileName):
        output_file = open(self.folderName+fileName, 'wb')
        for img in self.frameSeq:
            img.tofile(output_file)
        output_file.close()
        
    def makeLineSum(self,zRange=None):
        sizeY,sizeX=self.frameSeq[0].shape
        if zRange is None:
            zRange=range(0,len(self.frameSeq))
        
        yRange=range(sizeY)
        print("yRange: "+str(yRange))
        line3DSum=np.zeros(sizeY)
        lineMaxPos=[]
        numZ=0
        for z in zRange:
            lineSum=np.zeros(sizeY)
            for y in yRange:
                lineSumAct=np.mean(self.frameSeq[z][y,:])
                lineSum[y]=lineSumAct
                line3DSum[y]+=lineSumAct
            numZ+=1
            lineMaxPos.append(lineSum.argmax())
            
        line3DSum/=float(numZ)
        maxPosPx=np.mean(lineMaxPos)
        maxPos=self.indToPos(y=maxPosPx)
        clearNess=line3DSum[int(maxPosPx)]-np.mean(line3DSum)
        return maxPos,maxPosPx,clearNess
        
    def loadPoly(self,filename):
        poly2ndDegre=[]
        with open(filename, 'r') as fp:
            jsPoly=json.load(fp)
        for polyVal in jsPoly:
            poly2ndDegre.append(np.poly1d(polyVal))
        self.poly2ndDegre=poly2ndDegre
        return poly2ndDegre

        
    def savePoly(self,filename,poly2ndDegre=None):
    
        if poly2ndDegre is None:
            poly2ndDegre=self.poly2ndDegre
    
        polyVals=[]
        for poly in poly2ndDegre:
            polyVals.append(poly.coeffs.tolist())
        with open(self.folderName+filename, 'w') as fp:
            json.dump(polyVals, fp)
            
    def printDefinition(self):
        print("size ",str(self.imgShape))
        print("region ",str(self.imgRange))
    
    def showImage(self, ind,poly2ndDegre=None,poly=None,showPoly=False,offset=0,threshold=None):
    
        imarray=self.frameSeq[ind]
        if threshold:
            rgbimg = Image.fromarray(imarray).convert('RGB')
            for y in range(imarray.shape[0]):
                for x in range(imarray.shape[1]):
                    if imarray[y,x]>threshold:
                        rgbimg.putpixel((x,y),(255,0,0))
            plt.imshow(rgbimg)
        else:
            plt.imshow(imarray, cmap="gray")
        if showPoly:
            xRange=np.array(range(0,imarray.shape[1]))
            if poly is None:
                if poly2ndDegre is None:
                    poly2ndDegre=self.poly2ndDegre
                poly=self.getPoly(ind=ind,poly2ndDegre=poly2ndDegre)
            
            if not poly is None:
                plt.plot(xRange,self.posToInd(y=poly(self.indToPos(x=xRange)))+8+offset,'m')
                plt.plot(xRange,self.posToInd(y=poly(self.indToPos(x=xRange)))-8+offset,'m')
        plt.show()
    
    def plotPoly(self):
        xRange = np.linspace(self.imgRange[0][0], self.imgRange[0][1], 15)
        yRange = np.linspace(self.imgRange[2][0], self.imgRange[2][1], 15)

        zHeigts=np.zeros([len(xRange),len(yRange)])
        for indY,y in enumerate(yRange):
            poly=self.getPoly(zPos=y)
            for indX,x in enumerate(xRange):
                zHeigts[indY,indX]=poly(x)
        return xRange,yRange,zHeigts

    def __len__(self):
        return self._len

    @property
    def frame_shape(self):
        return self._frame_shape

    @property
    def pixel_type(self):
        return self._dtypedecaly
