import os

import ipywidgets as widgets
import glob

class MeasureBrowser(object):
    def __init__(self,path=os.getcwd()+'*/*'):
        self.path = path
        self._update_folders()
        self.folder=''
        self.folderPath=''
        self.file=''
        self.filePath=''
        self.changeEvent=None
        self.buttonEvent=None
        
        self._files=[]
        
    def _update_folders(self): 
        self.fileDict=dict()
        for fileName in glob.glob(self.path):
            folderList=fileName.split('/')
            #print('add '+folderList[-2]+'/'+folderList[-1])
            try:
                self.fileDict[folderList[-2]][folderList[-1]]=fileName
            except KeyError:
                self.fileDict[folderList[-2]]={folderList[-1]:fileName}
                
    def widget(self):
        box = widgets.VBox()
        self._update(box)
        return box
    
    def _update(self, box):
        def on_selFolder(name):
            self._files=sorted(list(self.fileDict[selFolder.value]))
            selFile.options=self._files
            selFile.value=self._files[0]
            on_selFile(None)
            
        def on_selFile(name):
            #print(self.fileDict)
            self.file=selFile.value
            self.folder=selFolder.value
            
            #print(self.file+'  '+self.folder)
            self.filePath=self.fileDict[self.folder][self.file]
            self.folderPath=self.filePath[:-len(self.file)]
            #print(self.filePath)
            if self.changeEvent:
                print(self.filePath)
                self.changeEvent(self)
                
        def openEvent(evt):
            if self.buttonEvent:
                self.buttonEvent(self)
        
        selFolder=widgets.Dropdown(options=sorted(list(self.fileDict)),description='Measure:',disabled=False)
        selFolder.observe(on_selFolder, names="value")
        
        selFile=widgets.Dropdown(options=self._files,description='File:',disabled=False)
        
        selFile.observe(on_selFile, names="value")
        on_selFolder(None)
        objList=[widgets.HTML("<h2>%s</h2>" % (self.path,)), selFolder,selFile]
        
        
        if self.buttonEvent:
            openBtn=widgets.Button(description='Open')
            openBtn.on_click(openEvent)
            objList.append(openBtn)
        box.children = objList
