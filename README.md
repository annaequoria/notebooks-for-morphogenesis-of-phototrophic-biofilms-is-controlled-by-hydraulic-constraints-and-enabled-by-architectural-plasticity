We obtained time-series of OCT scans reporting the structural development of phototrophic benthic biofilms in a open-channel flume experiment in raw lake water. Growth conditions and materials are reported in the relative publication. OCT scans at two lateral resolutions (40 um, 40 um, 2.18 um x,y,z respectivey) and (11.1 um ,11.1 um and 2.18 um) were taken every 3 days in a 15 days experiment. 66 x 3 low resolution (LR) scans covered an area of 0.4 m x 0.025 m in a tiled pattern. 3 x 3 high resolution (HR) scans were taken at the extremes of the same area of interest. These are labeled as slow-flow morphotype (SFM, or B5) and and fast-flow morphotype (FFM, or B1), to indicate the respective contrasting flow velocity conditions. The experiment was conducted in duplicate. Authomated OCT scans acquisition was done with the system descibed in (Depetris et al. 2019, Automated 3D Optical Coherence Tomography to Elucidate Biofilm Morphogenesis Over Large Spatial Scales, JoVE, 10.3791/59356). Data are available on Figshare (accession number).
This project contains jupyter notebooks for image acquisition, processing, analysis, as well as the analysis of the profiles.


ImageAcquisition.ipynb is the notebook used for the automated acquisition of OCT scans

OCTScans_Processing_LowResolution.ipynb was used to correct LR OCT scans, compute and stitch the DEMs

OCTScans_Processing_HighResolution.ipynb was used to correct HR OCT scans, compute and stitch the DEMs

Profiles_Prep.ipynb was used to prepare the profiles data and calculate gradients

MorphologicalGradients(Fig2).ipynb contains a moving window analysis of different morphological parameters that was performed on the LR DEMs

Davies_test_segmented_regression.ipynb contains the R code used for the segmented regression of the large morphological gradients

Morphogenesis(Fig3-Fig4).ipynb was used to analyse the time development of the biofilm morphology (on HR DEMs)

Stats-sliding speed.ipynb is the statistical analysis (in R) of the manual tracking of different biofilm structures over time

ChemMicroniche(Fig6).ipynb and ChemMicroniche(Fig6C).ipynb contain the R code used for profiles statistical analysis